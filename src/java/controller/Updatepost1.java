/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import Models.CategoryPost;
import Models.Post;
import dao.PostDao;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Admin
 */
public class Updatepost1 extends HttpServlet {

     @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        PostDao p = new PostDao();
        int postid = Integer.parseInt(request.getParameter("pid"));
        Post po = p.getinforbypost(postid);
        List<CategoryPost> listc = p.getCategoryPost();
        request.setAttribute("post", po);
        request.setAttribute("listc", listc);
        request.getRequestDispatcher("/Views/Updatepost.jsp").forward(request, response);

    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        PostDao p = new PostDao();
        
        String author = request.getParameter("author");
        int category = Integer.parseInt(request.getParameter("categoryid"));
        
        String content = request.getParameter("content");
        String discription = request.getParameter("description");
        String thumbnailLink = request.getParameter("thumbnailLink");
        String title = request.getParameter("title");
        String updatedate = request.getParameter("updatedate");
        CategoryPost cpid = new CategoryPost(category, "");
        Post po = new Post(title, content, discription, updatedate, thumbnailLink, author, cpid);
            p.getEditPost(po,category);
        response.sendRedirect("ManagerPostList1");

        
    }
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
