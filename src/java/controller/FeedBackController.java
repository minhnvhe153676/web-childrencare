/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import Models.CategoryService;
import Models.Service;
import Models.User;
import dao.ServiceDao;
import dao.userDao;
import java.io.IOException;
import java.io.PrintWriter;
import java.time.LocalDate;
import java.util.ArrayList;
import static java.util.Collections.list;
import java.util.List;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author Admin
 */
@WebServlet(name = "FeedBackDetail", urlPatterns = {"/FeedBackDetail"})
public class FeedBackController extends HttpServlet {



   
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
    }

   
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        request.setCharacterEncoding("UTF-8");
           HttpSession session = request.getSession();
        if (session.getAttribute("acc") == null) {
           response.sendRedirect("login.jsp");
        } else {
         User user = (User) session.getAttribute("acc");
         int id=user.getId();
            String Description = request.getParameter("txt");
            String fullname = request.getParameter("fullname");
            String mobile= request.getParameter("mobile");
            String email = request.getParameter("email");
            String idservice = request.getParameter("category");
            userDao dao = new userDao();
            ServiceDao service = new ServiceDao();
            List<CategoryService> list = service.getCategoryService();
           
            request.setAttribute("ListC",list );
           RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("/Views/Feedback.jsp");
       
        }
    }
}

   


