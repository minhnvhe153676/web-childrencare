/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import Models.CategoryPost;
import Models.CategoryService;
import Models.Post;
import Models.Service;
import dao.PostDao;
import dao.ServiceDao;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Admin
 */
public class ManageServiceListController extends HttpServlet {

   


    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
         
            String index = request.getParameter("Index");
            if(index == null){
                index = "1";
            }
            int indexPage = Integer.parseInt(index);
            
            ServiceDao sd = new ServiceDao();;
//           List<Post> listPost = pd.getAllPosts(0);
         
            List<Service> lists = sd.getAllService(indexPage);
            
            int count = sd.getTotalService();
            int endPage = count/6;
            if(count % 6 != 0){
                endPage++;
            }
                    
            
            request.setAttribute("indexPage", indexPage);
            request.setAttribute("listService", lists);
            request.setAttribute("numberOfPage", endPage);
            List<CategoryService> listc = sd.getCategoryService();
            request.setAttribute("listc", listc);
    
         RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("/Views/ManageServices.jsp");
        dispatcher.forward(request, response);
    }

    
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        ServiceDao sd = new ServiceDao();
        String title = request.getParameter("title");
        int categoryid = Integer.parseInt(request.getParameter("categoryid"));
        
        String Detail = request.getParameter("Details");
        Float quanlity = Float.parseFloat(request.getParameter("Quantity"));
        
        Float originalPrice = Float.parseFloat(request.getParameter("originalPrice"));
        String Images = request.getParameter("Images");
        String updatedate = request.getParameter("updatedate");   
           CategoryService cpid = new CategoryService(categoryid, "");
           Service s = new Service(title, Detail, quanlity, Images, cpid, updatedate, categoryid);
            sd.getAddService(s, categoryid);
        response.sendRedirect("ManagerServiceList");
        
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
