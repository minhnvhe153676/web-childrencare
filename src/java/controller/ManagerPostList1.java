/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import Models.CategoryPost;
import Models.Post;
import dao.PostDao;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Admin
 */
public class ManagerPostList1 extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");

    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        String index = request.getParameter("Index");
        if (index == null) {
            index = "1";
        }
        int indexPage = Integer.parseInt(index);

        PostDao pd = new PostDao();;
//           List<Post> listPost = pd.getAllPosts(0);

        List<Post> listp = pd.getAllPosts(indexPage);

        int count = pd.getTotalPost();
        int endPage = count / 6;
        if (count % 6 != 0) {
            endPage++;
        }

        request.setAttribute("indexPage", indexPage);
        request.setAttribute("listPost", listp);
        request.setAttribute("numberOfPage", endPage);
        List<CategoryPost> listc = pd.getCategoryPost();
        request.setAttribute("listc", listc);
        RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("/Views/ManagePostList.jsp");
        dispatcher.forward(request, response);

    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        PostDao p = new PostDao();

        String author = request.getParameter("author");
        int categoryid = Integer.parseInt(request.getParameter("categoryid"));

        String content = request.getParameter("content");
        String discription = request.getParameter("description");
        String thumbnailLink = request.getParameter("thumbnailLink");
        String title = request.getParameter("title");
        String updatedate = request.getParameter("updatedate");
        CategoryPost cpid = new CategoryPost(categoryid, "");
        Post po = new Post(title, content, discription, updatedate, thumbnailLink, author, cpid);
        p.getAddPost(po, categoryid);
        response.sendRedirect("ManagerPostList1");

    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
