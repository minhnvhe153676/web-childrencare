/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import Models.Post;
import dao.PostDao;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.ParseException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Nguyen Minh Hoang
 */
public class SearchPostByTittleController extends HttpServlet {
    
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException, ParseException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        PostDao pd = new PostDao();
        String txt = request.getParameter("txt");
        List<Post> listPost = pd.getPostsByTitle(txt);
        for (Post post : listPost) {
            out.print("<div class=\"single-blog-post col-xl-6 col-lg-6 col-md-12 col-sm-12\" style=\"justify-content: space-around;margin-bottom: 0; \"   >                                        \n"
                    + "                                        <div class=\"img-holder\"style=\"width: 100%; height: 200px;  \">\n"
                    + "                                            <img src=\"" + post.getThumbnailLink() + "\" alt=\"Awesome Image\">\n"
                    + "                                            <div class=\"categorie-button\">\n"
                    + "                                                <a class=\"btn-one\" href=\"#\">" + post.getCategory() + "</a>    \n"
                    + "                                            </div>\n"
                    + "                                        </div>\n"
                    + "                                        <div class=\"text-holder\"style=\"width: 100%; height: 400px;\">\n"
                    + "                                            <div class=\"meta-box\">\n"
                    + "                                                <div class=\"author-thumb\">\n"
                    + "                                                    <img src=\"./images/blog/author-2.png\" alt=\"Image\">\n"
                    + "                                                </div>\n"
                    + "                                                <ul class=\"meta-info\">\n"
                    + "                                                    <li><a href=\"#\">" + post.getAuthor() + "</a></li>\n"
                    + "                                                    <li><a href=\"#\">" + post.getDatePublic2() + "</a></li>\n"
                    + "                                                </ul>    \n"
                    + "                                            </div>\n"
                    + "                                            <h3 class=\"blog-title\"><a href=\"PostDetail?index=" + post.getId() + "\">" + post.getTitle() + "</a></h3> \n"
                    + "                                            <div class=\"text-box\">\n"
                    + "                                                <p>" + post.getContent() + "</p>\n"
                    + "                                            </div>\n"
                    + "                                            <div class=\"readmore-button\">\n"
                    + "                                                <a class=\"btn-two\" href=\"PostDetail?index=" + post.getId() + "\"><span class=\"flaticon-next\"></span>Continue Reading</a>\n"
                    + "                                            </div>  \n"
                    + "                                        </div>   \n"
                    + "                                    </div>");
        }
        
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (ParseException ex) {
            Logger.getLogger(SearchPostByTittleController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (ParseException ex) {
            Logger.getLogger(SearchPostByTittleController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
