/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import Models.User;
import dao.userDao;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Date;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author Admin
 */
@WebServlet(name = "ProfileDetail", urlPatterns = {"/ProfileDetail"})
public class ProfileDetail extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        request.setCharacterEncoding("UTF-8");
      
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        request.setCharacterEncoding("UTF-8");
         HttpSession session = request.getSession();
        if (session.getAttribute("acc") == null) {
            response.sendRedirect("Login.jsp");
        } else {
         User user = (User) session.getAttribute("acc");
//        User user = new User("Huy", "minh@gmai.com", "123456", "huy", true, "1234567", address, role, 0, dob, image)
         
        int id = user.getId();
        String fullname = request.getParameter("fullname");
        String username = request.getParameter("username");
        
        String email = request.getParameter("email");
        String sDob = request.getParameter("dob");
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
        Date Dob=null;
        try {
            Dob = (Date) sdf.parse(sDob);
        } catch (ParseException ex) {
            Logger.getLogger(ProfileDetail.class.getName()).log(Level.SEVERE, null, ex);
        }
       
        Boolean gender = Boolean.parseBoolean("gender");
        String mobile = request.getParameter("mobile");
        
        userDao dao = new userDao();
        dao.UpdateUser(fullname, mobile, username, email, Dob, gender, sDob, id);
        request.setAttribute("mess2", "Update success");
        request.getRequestDispatcher("Profile.jsp").forward(request, response);
//        }
        
        
        }
    }

}
