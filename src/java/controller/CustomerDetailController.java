/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import Models.Role;
import Models.User;
import dao.userDao;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Nguyen Minh Hoang
 */
public class CustomerDetailController extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        int id = 0;
        if (request.getParameter("id") != null) {
              userDao ud = new userDao();
              id = Integer.parseInt(request.getParameter("id"));
            if(request.getParameter("action")!=null){
                ud.getDeleteCustomer(id);
                response.sendRedirect("Customer");
                return;
            }
            User u = ud.getCustomerDetail(id);
            request.setAttribute("user", u);
            request.setAttribute("unknown", "none");
        } else {

            request.setAttribute("AddNew", 1);
        }

        userDao ud = new userDao();
        User user = ud.getCustomerDetail(id);
        request.setAttribute("User", user);
        RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("/Views/CustomerDetail.jsp");
        dispatcher.forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        
        userDao ud = new userDao();
        String username = request.getParameter("username");
        String email = request.getParameter("email");
        String password = request.getParameter("password");
        String fullName = request.getParameter("FullName");
        boolean gender = Boolean.parseBoolean(request.getParameter("gender"));
        String mobile = request.getParameter("phone");
        String address = request.getParameter("address");
        int status = Integer.parseInt(request.getParameter("status"));
        String image = request.getParameter("img");
        if(request.getParameter("checkAdd").equals("update")){
             int id = Integer.parseInt(request.getParameter("id"));
           
            User u = new User(username, email, password, fullName, gender, mobile, address, status, image);
            ud.getEditCustomer(id, u);
        }else if(request.getParameter("checkAdd").equals("add")){
            int roleId = Integer.parseInt(request.getParameter("role"));
            Role r = new Role(roleId, "");
            User u = new User(username, email, password, fullName, gender, mobile, address, status, image,r);
            ud.getAddCustomer(u, roleId);
        }
           
            
        

        response.sendRedirect("Customer");
    }

}
