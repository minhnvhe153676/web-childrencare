/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Models;

import java.sql.Date;

/**
 *
 * @author Dell
 */
public class ReservationService {
    private Reservation reservation;
    private Service service;
    private int status;
    private Date dob;

    public ReservationService() {
    }

    public ReservationService(Reservation reservation, Service service, int status, Date dob) {
        this.reservation = reservation;
        this.service = service;
        this.status = status;
        this.dob = dob;
    }

    public Reservation getReservation() {
        return reservation;
    }

    public void setReservation(Reservation reservation) {
        this.reservation = reservation;
    }

    public Service getService() {
        return service;
    }

    public void setService(Service service) {
        this.service = service;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public Date getDob() {
        return dob;
    }

    public void setDob(Date dob) {
        this.dob = dob;
    }
    
    
}
