/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Models;

import java.util.ArrayList;
import java.util.Date;

/**
 *
 * @author Dell
 */
public class Reservation {
    private int id;
    private User customer;
    private Date reservationDate;
    private Date checkupTime;
    private User staff;
    private float totalCost;

    public Reservation() {
    }

    public Reservation(int id, User customer, Date reservationDate, Date checkupTime, User staff, float totalCost) {
        this.id = id;
        this.customer = customer;
        this.reservationDate = reservationDate;
        this.checkupTime = checkupTime;
        this.staff = staff;
        this.totalCost = totalCost;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public User getCustomer() {
        return customer;
    }

    public void setCustomer(User customer) {
        this.customer = customer;
    }

    public Date getReservationDate() {
        return reservationDate;
    }

    public void setReservationDate(Date reservationDate) {
        this.reservationDate = reservationDate;
    }

    public Date getCheckupTime() {
        return checkupTime;
    }

    public void setCheckupTime(Date checkupTime) {
        this.checkupTime = checkupTime;
    }

    public User getStaff() {
        return staff;
    }

    public void setStaff(User staff) {
        this.staff = staff;
    }

    public float getTotalCost() {
        return totalCost;
    }

    public void setTotalCost(float totalCost) {
        this.totalCost = totalCost;
    }
    
    
}
