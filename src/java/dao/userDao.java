/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import Models.Post;
import Models.Role;
import Models.User;
import context.DBContext;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;


public class userDao {
    
      DBContext bContext;

    public userDao() {
        this.bContext = new DBContext();
    }
    Connection con;
    PreparedStatement ps;
    ResultSet rs;
    
    public User getCustomerDetail(int id) {
        User u = new User();
        try {

            String querry = "SELECT u.Gender,u.FullName,u.Email,u.PhoneNumber,u.UserName,u.[Password]\n" +
"		,u.[Status],u.[Address] FROM dbo.Users AS u WHERE u.ID = ?";
            con = bContext.getConnection();
            ps = con.prepareStatement(querry);
            ps.setInt(1, id);
            rs = ps.executeQuery();
            while (rs.next()) {
               
                u.setAddress(rs.getString("Address"));
                u.setEmail(rs.getString("Email"));
                u.setGender(rs.getBoolean("Gender"));
                //u.setId(rs.getInt("ID"));
                u.setFullName(rs.getString("FullName"));
                u.setMobile(rs.getString("PhoneNumber"));
                u.setUsername(rs.getString("UserName"));
                u.setPassword(rs.getString("Password"));
                u.setRole(new Role(rs.getInt("RoleId"), "Customer"));
                u.setStatus(rs.getInt("Status"));
                //u.setDob(rs.getString("Dob"));
                //u.setImage(rs.getString("Image"));
               
            }
        } catch (Exception e) {
            e.getMessage();
        } finally {
            bContext.closeResources(con, ps, rs);
        }
        return u;
    }
     public List<User> getCustomersToPaging(int pageNumber) {
        List<User> listCustomer = new ArrayList<User>();
        try {

            String querry = "SELECT u.Image,u.ID, u.FullName,u.Gender,u.Email,u.PhoneNumber,u.[Status]\n" +
"		FROM dbo.Users AS u WHERE u.RoleId = 2\n" +
"		ORDER BY u.ID asc OFFSET ? ROWS FETCH NEXT 5 ROWS ONLY ;;";
            con = bContext.getConnection();
            ps = con.prepareStatement(querry);
            ps.setInt(1, pageNumber);
            rs = ps.executeQuery();
            while (rs.next()) {
                User u = new User();
                //u.setAddress(rs.getString("Address"));
                u.setEmail(rs.getString("Email"));
                u.setId(rs.getInt("ID"));
                u.setFullName(rs.getString("FullName"));
                u.setMobile(rs.getString("PhoneNumber"));
                //u.setUsername(rs.getString("UserName"));
                //u.setPassword(rs.getString("Password"));
                //u.setRole(new Role(rs.getInt("RoleId"), "Customer"));
                u.setStatus(rs.getInt("Status"));
                //u.setDob(rs.getString("Dob"));
                u.setGender(rs.getBoolean("Gender"));
                u.setImage(rs.getString("Image"));
                listCustomer.add(u);
            }
        } catch (Exception e) {
            e.getMessage();
        } finally {
            bContext.closeResources(con, ps, rs);
        }
        return listCustomer;
    }
     
    //count total of customers
      public int getTotalCustomer() {
        int result = 0;
        try {
            String querry = "SELECT COUNT(ID) as total FROM dbo.Users WHERE RoleId = 2";
            con = bContext.getConnection();
            ps = con.prepareStatement(querry);
            rs = ps.executeQuery();
            rs.next();
            result = Integer.parseInt(rs.getString("total"));
        } catch (Exception e) {
            e.getMessage();
        } finally {
            bContext.closeResources(con, ps, rs);
        }
        return result;
    }
    public User loginAccount(String email, String password) {
        User account = null;
        String query = "select * from [dbo].[Users] where email = ? and password = ? ";
        try {
            con = new DBContext().getConnection();//mo ket noi voi sql
            ps = con.prepareStatement(query);
            ps.setString(1, email);
            ps.setString(2, password);
            rs = ps.executeQuery();
            while (rs.next()) {
                 User u = new User();
                u.setAddress(rs.getString("Address"));
                u.setEmail(rs.getString("Email"));
                u.setId(rs.getInt("ID"));
                u.setFullName(rs.getString("FullName"));
                u.setMobile(rs.getString("PhoneNumber"));
                u.setUsername(rs.getString("UserName"));
                u.setPassword(rs.getString("Password"));
                u.setRole(new Role(rs.getInt("RoleId"), "Customer"));
                u.setStatus(rs.getInt("Status"));
                u.setDob(rs.getString("Dob"));
                u.setGender(rs.getBoolean("Gender"));
                u.setImage(rs.getString("Image"));
            }
        } catch (Exception e) {
            System.err.println(e.getMessage());
        }
        return account;
    }
     
     
    //update customer
    public void getEditCustomer(int id, User u) {

        try {
            String querry = "UPDATE dbo.Users SET Gender = ?,FullName = ?,Email = ?,\n" +
"		PhoneNumber = ?,UserName = ?,[Password] = ?,[Status]=?,[Address]=?\n" +
"		,[Image]=? WHERE ID =?";
            con = bContext.getConnection();
            ps = con.prepareStatement(querry);
            ps.setInt(1, u.isGender()==true?1:0);
            ps.setString(2, u.getFullName());
            ps.setString(3, u.getEmail());
            ps.setString(4, u.getMobile());
            ps.setString(5, u.getUsername());
            ps.setString(6, u.getPassword());
            ps.setInt(7, u.getStatus());
            ps.setString(8, u.getAddress());
            ps.setString(9, "./images/users/"+u.getImage());
            ps.setInt(10, id);
            ps.executeUpdate();

        } catch (Exception e) {
            e.getMessage();
        } finally {
            bContext.closeResources(con, ps, rs);
        }

    }
    
    public void getAddCustomer(User u,int roleId) {

        try {
            String querry = "INSERT INTO dbo.Users\n" +
"		VALUES\n" +
"		(   ?,    -- FullName - nvarchar(100)\n" +
"		    ?,    -- PhoneNumber - varchar(50)\n" +
"		    ?,    -- UserName - varchar(100)\n" +
"		    ?,    -- Email - varchar(100)\n" +
"		    ?,    -- Password - varchar(100)\n" +
"		    ?,    -- RoleId - int\n" +
"		    ?,    -- Status - int\n" +
"		    DEFAULT, -- Dob - datetime\n" +
"		    ?,    -- Gender - bit\n" +
"		    ?,    -- Address - nvarchar(100)\n" +
"		    ?     -- Image - nvarchar(200)\n" +
"		    )";
            con = bContext.getConnection();
            ps = con.prepareStatement(querry);
            ps.setInt(8, u.isGender()==true?1:0);
            ps.setString(1, u.getFullName());
            ps.setString(4, u.getEmail());
            ps.setString(2, u.getMobile());
            ps.setString(3, u.getUsername());
            ps.setString(5, u.getPassword());
            ps.setInt(7, u.getStatus());
            ps.setString(9, u.getAddress());
            ps.setString(10, u.getImage());
            ps.setInt(6, u.getRole().getId());
            ps.executeUpdate();

        } catch (Exception e) {
            e.getMessage();
        } finally {
            bContext.closeResources(con, ps, rs);
        }

    }
     public List<Role> getAllRole() {
        List<Role> listRole = new ArrayList<Role>();
        try {

            String querry = "SELECT * FROM dbo.Roles;";
            con = bContext.getConnection();
            ps = con.prepareStatement(querry);
            rs = ps.executeQuery();
            while (rs.next()) {
               Role r = new Role();
               r.setId(rs.getInt("ID"));
               r.setName(rs.getString("Name"));
               listRole.add(r);
            }
        } catch (Exception e) {
            e.getMessage();
        } finally {
            bContext.closeResources(con, ps, rs);
        }
        return listRole;
    }
    public void getDeleteCustomer(int id) {

        try {
            String querry = "UPDATE dbo.Users SET [Status] = 4 WHERE ID = ?;";
            con = bContext.getConnection();
            ps = con.prepareStatement(querry);
            ps.setInt(1, id);          
            ps.executeUpdate();

        } catch (Exception e) {
            e.getMessage();
        } finally {
            bContext.closeResources(con, ps, rs);
        }

    }
         public void UpdateUser(String fullname,String mobile,String username,String email,Date dob,Boolean gender,String address,int id) {
      
        String sql = "update Users\n" +
                     "set [FullName]= ?,\n" +
                     "[PhoneNumber]=?,\n" +
                     "[UserName]=?,\n" +
                     "[Email]=?,\n" +
                     "[Dob]=?,\n" +
                     "[Gender]=?\n" +
                     "[Address]=?\n" +
                     "where [id]=?";
        try {
            PreparedStatement ps = con.prepareStatement(sql);
            ps.setString(1, fullname);
            ps.setString(2, mobile);
            ps.setString(3, username);
            ps.setString(4,email );
            ps.setDate(5, dob);
            ps.setBoolean(6, gender);
            ps.setString(7, address);
            ps.setInt(8, id);
            ps.executeUpdate();
        } catch (SQLException e) {
            System.out.println(e);
        }
    
    }
    public int insertFeedBack(String Description,String mobile, String Fullname,String email, int userid, int Idservice){
        String sql = "insert [Feedback] ([Description],[Mobile],[FullName][Email][id],[user]) values (?,?,?,?)";
        try {
            PreparedStatement ps = con.prepareStatement(sql);
            ps.setString(1,Description);
            ps.setString(2, mobile);
            ps.setString(3,Fullname);
            ps.setString(4, email);
            ps.setInt(5, userid);
            ps.setInt(6, Idservice);
            return ps.executeUpdate();
        } catch (SQLException e) {
            System.out.println(e);
            return 0;
        }
    }
    public User getUserbyIDUser(int id){
         

        
        User u = new User();
        try {

            String querry = "SELECT u.Gender,u.FullName,u.Email,u.PhoneNumber,u.UserName,u.[Address]\n" +
"		,u.[Address] FROM dbo.Users AS u WHERE u.ID = ?";
            con = bContext.getConnection();
            ps = con.prepareStatement(querry);
            ps.setInt(1, id);
            rs = ps.executeQuery();
            while (rs.next()) {
               
                u.setAddress(rs.getString("Address"));
                u.setEmail(rs.getString("Email"));
                u.setGender(rs.getBoolean("Gender"));
                //u.setId(rs.getInt("ID"));
                u.setFullName(rs.getString("FullName"));
                u.setMobile(rs.getString("PhoneNumber"));
                u.setUsername(rs.getString("UserName"));
//                u.setPassword(rs.getString("Password"));
                //u.setRole(new Role(rs.getInt("RoleId"), "Customer"));
                //u.setStatus(rs.getInt("Status"));
                u.setDob(rs.getString("Dob"));
                //u.setImage(rs.getString("Image"));
               
            }
        } catch (Exception e) {
            e.getMessage();
        } finally {
            bContext.closeResources(con, ps, rs);
        }
        return u;
    }
     public static void main(String[] args) {
          userDao u = new userDao();
          Role role = new Role(1, "");
        User us = new User("minh", "minh@gmail", "123", "minhnguyen", true, "0123456789", "thanhhoas", 0, "", role);
        u.getAddCustomer(us, 1);
        
           }
}
