
package dao;

import Models.CategoryPost;
import context.DBContext;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;
import Models.CategoryService;
import Models.Post;
import Models.Service;

public class ServiceDao {

    DBContext bContext;
    public ServiceDao() {
        this.bContext = new DBContext();
    }
    Connection con;
    PreparedStatement ps;
    ResultSet rs;
    
    //list all category service
      public List<CategoryService> getCategoryService() {
          List<CategoryService> list= new ArrayList<CategoryService>();
        try {
           
            String querry = "SELECT * FROM dbo.CategoryService";
            con = bContext.getConnection();
            ps = con.prepareStatement(querry);
            rs = ps.executeQuery();
            while (rs.next()) {
               CategoryService cs = new CategoryService();
              cs.setId(rs.getInt("ID"));
              cs.setName(rs.getString("CategoryName"));
              cs.setThumbnail(rs.getString("thumbnail"));
              cs.setShortDescription(rs.getString("ShortDescription"));
               list.add(cs);
            }
        } catch (Exception e) {
            e.getMessage();
        } finally {
            bContext.closeResources(con, ps, rs);
        }
        return list;
    }
      //list 6 Service by pagination
    public List<Service> getAllService(int pageNumber) {
        List<Service> listService = new ArrayList<Service>();
        try {

            String querry = "SELECT  s.ID,s.Name,s.CategoryId,s.Images,s.OriginalPrice,cs.CategoryName,s.Details,s.Quantity,s.UpdatedDate\n" +
                            "FROM dbo.Service AS s INNER JOIN dbo.CategoryService AS cs ON cs.ID = s.CategoryId\n" +
                            "ORDER BY s.UpdatedDate DESC OFFSET ? ROWS FETCH NEXT 6 ROWS ONLY";
            con = bContext.getConnection();
            ps = con.prepareStatement(querry);
            ps.setInt(1, (pageNumber-1)*6);
            rs = ps.executeQuery();
            while (rs.next()) {
                Service s = new Service();
                s.setId(rs.getInt("ID"));
                s.setFullname(rs.getString("Name"));
                s.setCategory(new CategoryService(rs.getInt("CategoryId"),rs.getString("CategoryName")));
                s.setDetails(rs.getString("Details"));
                s.setQuantity(rs.getInt("Quantity"));
                s.setOriginalPrice(rs.getFloat("OriginalPrice"));
                s.setThumbnailLink(rs.getString("Images"));
                s.setUpdatedDate(rs.getString("UpdatedDate"));
                listService.add(s);
            }
        } catch (Exception e) {
            e.getMessage();
        } finally {
            bContext.closeResources(con, ps, rs);
        }
        return listService;
    }
    public List<Service> getInformationService(int id) {
        List<Service> listService = new ArrayList<Service>();
        try {

            String querry = "SELECT  s.ID,s.Name,s.CategoryId,s.Images,s.OriginalPrice,cs.CategoryName,s.Details,s.Quantity,s.UpdatedDate\n" +
"                            FROM dbo.Service AS s INNER JOIN dbo.CategoryService AS cs ON cs.ID = s.CategoryId";
            con = bContext.getConnection();
            ps = con.prepareStatement(querry);
            ps.setInt(1, id);
            rs = ps.executeQuery();
            while (rs.next()) {
                Service s = new Service();
                s.setId(rs.getInt("ID"));
                s.setFullname(rs.getString("Name"));
                s.setCategory(new CategoryService(rs.getInt("CategoryId"),rs.getString("CategoryName")));
                s.setDetails(rs.getString("Details"));
                s.setQuantity(rs.getInt("Quantity"));
                s.setOriginalPrice(rs.getFloat("OriginalPrice"));
                s.setThumbnailLink(rs.getString("Images"));
                s.setUpdatedDate(rs.getString("UpdatedDate"));
                listService.add(s);
            }
        } catch (Exception e) {
            e.getMessage();
        } finally {
            bContext.closeResources(con, ps, rs);
        }
        return listService;
    }
    public int getTotalService() {
        int result = 0;
        try {
            String querry = "SELECT COUNT(dbo.Service.ID)AS totalPosts FROM dbo.Service";
            con = bContext.getConnection();
            ps = con.prepareStatement(querry);
            rs = ps.executeQuery();
            rs.next();
            result = Integer.parseInt(rs.getString("totalPosts"));
        } catch (Exception e) {
            e.getMessage();
        } finally {
            bContext.closeResources(con, ps, rs);
        }
        return result;
    }
    
 public void getAddService(Service p, int csid) {

        try {
            String querry = "INSERT INTO dbo.Service\n" +
"		VALUES\n" +
"		   (?,    -- CategoryId - int\n" +   
"                   ?,    -- Images - nvarchar(200)\n" +
"		    ?,    -- OriginalPrice - float\n" +
"		    ?,    -- Details - nvarchar(max)\n" +
"		    ?,    -- UpdatedDate - datetime\n" +
"		    ?,    -- Quantity - int\n" +
"		    )";
            con = bContext.getConnection();
            ps = con.prepareStatement(querry);
            ps.setInt(1, p.getCategory().getId());
            ps.setString(2, p.getThumbnailLink());
            ps.setString(5, p.getUpdatedDate());
            ps.setFloat(3, p.getOriginalPrice());
            ps.setString(4, p.getDetails());
            ps.setInt(6, p.getQuantity());

             
            ps.executeUpdate();

        } catch (Exception e) {
            e.getMessage();
        } finally {
            bContext.closeResources(con, ps, rs);
        }
     }
      public static void main(String[] args) {
          ServiceDao s = new ServiceDao();
        List<Service> list = s.getAllService(0);
       
        
        
          for (Service cs : list) {
              System.out.println(cs.getDetails());
          }
    }
}

