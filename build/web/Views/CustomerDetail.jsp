<%-- 
    Document   : CustomerDetail
    Created on : May 25, 2022, 3:34:27 PM
    Author     : Nguyen Minh Hoang
--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="en">

    <!-- Mirrored from st.ourhtmldemo.com/new/Dento/blog.html by HTTrack Website Copier/3.x [XR&CO'2014], Tue, 05 Jan 2021 02:07:28 GMT -->
    <head>
        <meta charset="UTF-8">
        <title>Blog</title>


        <jsp:include page="/Views/LinkHeader.jsp"/>
        <jsp:useBean id="daoPost" class="dao.PostDao" scope="request"/>
        <jsp:useBean id="daoService" class="dao.ServiceDao" scope="request"/>
        <jsp:useBean id="daoUser" class="dao.userDao" scope="request"/>
        <style>
            body{
                margin-top:20px;
                background:#f5f7fa;
            }
            .panel.panel-default {
                border-top-width: 3px;
            }
            .panel {
                box-shadow: 0 3px 1px -2px rgba(0,0,0,.14),0 2px 2px 0 rgba(0,0,0,.098),0 1px 5px 0 rgba(0,0,0,.084);
                border: 0;
                border-radius: 4px;
                margin-bottom: 16px;
            }
            .thumb96 {
                width: 96px!important;
                height: 96px!important;
            }
            .thumb48 {
                width: 48px!important;
                height: 48px!important;
            }
        </style>
    </head>

    <body>
        <%@include file="/Views/Header.jsp" %>   

        <!--Start breadcrumb area-->     
        <section class="breadcrumb-area" style="background-image: url(./images/resources/breadcrumb-bg.jpg); margin-bottom: 30px;" >
            <div class="container">
                <div class="row">
                    <div class="col-xl-12">
                        <div class="inner-content clearfix">
                            <div class="title float-left">
                                <h2>Post</h2>
                            </div>
                            <div class="breadcrumb-menu float-right">
                                <ul class="clearfix">
                                    <li><a href="Homepage.jsp">Home</a></li>
                                    <li><i class="fa fa-angle-right" aria-hidden="true"></i></li>
                                    <li class="active">Customer Detail</li>
                                </ul>    
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <div class="container bootstrap snippets bootdey">
            <div class="row ng-scope">
                <div class="col-md-4">
                    <div class="panel panel-default">
                        <div class="panel-body text-center">
                            <div class="pv-lg"><img class="center-block img-responsive img-circle img-thumbnail thumb96" src="https://bootdey.com/img/Content/avatar/avatar1.png" alt="Contact"></div>
                            <h3 class="m0 text-bold">Audrey Hunt</h3>
                            <div class="mv-lg">
                                <p>Hello, I'm a just a dummy contact in your contact list and this is my presentation text. Have fun!</p>
                            </div>
                            <div class="text-center"><a class="btn btn-primary" href="">Send message</a></div>
                        </div>
                    </div>
                    <div class="panel panel-default hidden-xs hidden-sm">
                        <div class="panel-heading">
                            <div class="panel-title text-center">Recent contacts</div>
                        </div>
                        <div class="panel-body">
                            <div class="media">
                                <div class="media-left media-middle">
                                    <a href="#"><img class="media-object img-circle img-thumbnail thumb48" src="https://bootdey.com/img/Content/avatar/avatar2.png" alt="Contact"></a>
                                </div>
                                <div class="media-body pt-sm">
                                    <div class="text-bold">Floyd Ortiz
                                        <div class="text-sm text-muted">12m ago</div>
                                    </div>
                                </div>
                            </div>
                            <div class="media">
                                <div class="media-left media-middle">
                                    <a href="#"><img class="media-object img-circle img-thumbnail thumb48" src="https://bootdey.com/img/Content/avatar/avatar3.png" alt="Contact"></a>
                                </div>
                                <div class="media-body pt-sm">
                                    <div class="text-bold">Luis Vasquez
                                        <div class="text-sm text-muted">2h ago</div>
                                    </div>
                                </div>
                            </div>
                            <div class="media">
                                <div class="media-left media-middle">
                                    <a href="#"><img class="media-object img-circle img-thumbnail thumb48" src="https://bootdey.com/img/Content/avatar/avatar4.png" alt="Contact"></a>
                                </div>
                                <div class="media-body pt-sm">
                                    <div class="text-bold">Duane Mckinney
                                        <div class="text-sm text-muted">yesterday</div>
                                    </div>
                                </div>
                            </div>
                            <div class="media">
                                <div class="media-left media-middle">
                                    <a href="#"><img class="media-object img-circle img-thumbnail thumb48" src="https://bootdey.com/img/Content/avatar/avatar5.png" alt="Contact"></a>
                                </div>
                                <div class="media-body pt-sm">
                                    <div class="text-bold">Connie Lambert
                                        <div class="text-sm text-muted">2 weeks ago</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-8">
                    <div class="panel panel-default">
                        <div class="panel-body">

                            <div class="h4 text-center">Customer Detail</div>
                            <div class="row pv-lg">
                                <div class="col-lg-2"></div>
                                <div class="col-lg-8">
                                    <form class="form-horizontal ng-pristine ng-valid" method="post">
                                        <div class="form-group">
                                            <label class="col-sm-2 control-label" for="inputContact3">FullName</label>
                                            <div class="col-sm-10">
                                                <input class="form-control" name="FullName" id="inputContact4" type="text" value="${user.getFullName()}">
                                            </div>
                                        </div>
                                        <div class="form-group" style="display: flex; justify-content: space-between;">
                                            <label  class="col-sm-2 control-label" for="male">Male</label>
                                            <div class="col-sm-5">
                                                <input type="radio" name="gender" id="male" value="true" <c:if test="${user.isGender()}">checked</c:if>>
                                                </div>
                                                <label  class="col-sm-2 control-label" for="female">Female</label>
                                                <div class="col-sm-5">
                                                    <input type="radio" name="gender" id="female" value="false" <c:if test="${user.isGender()==false}">checked</c:if>>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-sm-2 control-label" for="inputContact2">Email</label>
                                                <div class="col-sm-10">
                                                    <input class="form-control" name="email" id="inputContact2" type="email" value="${user.getEmail()}">
                                            </div>
                                        </div>
                                        <div class="form-group" style="display: ${unknown==null?block:unknown}">
                                            <label class="col-sm-2 control-label" for="role">Role ID</label>
                                            <div class="col-sm-10">
                                                <select id="role" name="role"class="form-control">
                                                     <option selected value="2">Customer</option>   
                                                    <%--<c:forEach var="x" items="${daoUser.allRole}">
                                                        <option value="${x.id}">${x.name}</option>   
                                                    </c:forEach>--%>

                                                </select>
                                            </div>

                                        </div>
                                        <div class="form-group">
                                            <label class="col-sm-2 control-label" for="inputContact4">Phone</label>
                                            <div class="col-sm-10">
                                                <input class="form-control"name="phone" id="inputContact3" type="text" value="${user.getMobile()}">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-sm-2 control-label" for="inputContact5">UserName</label>
                                            <div class="col-sm-10">
                                                <input class="form-control" name="username" id="inputContact4" type="text" value="${user.getUsername()}">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-sm-2 control-label" for="inputContact6">Password</label>
                                            <div class="col-sm-10">
                                                <input class="form-control" name="password" id="inputContact5" type="text" value="${User.getPassword()}">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-sm-2 control-label" for="inputContact7">Status</label>
                                            <div class="col-sm-10">
                                                <input class="form-control" id="inputContact5" name="status" type="number" pattern = "[1-4]{1}"
                                                       title='Must in range 1 -> 4' value="${user.getStatus()}">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-sm-2 control-label" for="inputContact8">Address</label>
                                            <div class="col-sm-10">
                                                <input class="form-control" name="address" id="inputContact7" type="text" value="${user.getAddress()}">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-sm-2 control-label" for="inputContact9">Image</label>
                                            <div class="col-sm-10">
                                                <input id="picture" type="file" name="picture" id="imputContact8" accept="image/*" required="" value="" />
                                                <input style="display: none;" id="empty" type="text" value="" name="img"/>
                                            </div>
                                        </div>
                                            <input style="display: none;" id="updateAdd" type="text"  name="checkAdd"/><!--check-->
                                        <div class="form-group">
                                            <div class="col-sm-offset-2 col-sm-10">
                                                <button id="submit" onclick="submitForm()" class="btn btn-info" type="submit">
                                                    <c:choose>
                                                        <c:when test="${AddNew==1}"> 
                                                            Add New Customer
                                                        </c:when>
                                                        <c:otherwise>
                                                            Update
                                                        </c:otherwise>

                                                    </c:choose> </button>
                                            </div>
                                        </div>
                                    </form>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <%@include file="/Views/Footer.jsp" %>
        <!-- /.End Of Color Palate -->
        <jsp:include page="/Views/LinkFooter.jsp"/>

    </body>
    <script>
        function  submitForm() {
            var image = document.getElementById("picture").value;
            var checkSubmit = document.getElementById("submit").innerHTML;
            //var empty = document.getElementById("empty");
            var words = image.split('\\');
            //empty.innerHTML=words[words.length - 1];
            document.getElementById("empty").setAttribute('value', words[words.length - 1]);
            if (checkSubmit.includes("Update")) {
                 document.getElementById("updateAdd").setAttribute('value',"update");
                confirm("Do you want to edit customer?");
                //console.log(words[words.length - 1]);
            } else {
//                var input = document.createElement("input");
//                input.type = "text";;
//                input.className = "checkAdd"; // set the CSS class
//                input.style.display = none;
//                input.name = "checkAdd";
//                input.value = "addMore";
//                container.appendChild(input); // put it into the DOM
                document.getElementById("updateAdd").setAttribute('value',"add");
                confirm("Do you want to add more customer?");
            }




        }
    </script>

</html>
