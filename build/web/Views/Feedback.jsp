<%-- 
    Document   : PostDetail
    Created on : May 16, 2022, 10:59:43 PM
    Author     : Nguyen Minh Hoang
--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="en">

    <!-- Mirrored from st.ourhtmldemo.com/new/Dento/blog-single.html by HTTrack Website Copier/3.x [XR&CO'2014], Tue, 05 Jan 2021 02:08:35 GMT -->
    <head>
        <meta charset="UTF-8">
        <title>Feedback</title>

        <!-- responsive meta -->
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- For IE -->
        <meta http-equiv="X-UA-Compatible" content="IE=edge">

        <jsp:include page="/Views/LinkHeader.jsp"/>

    </head>

    <body>
        <jsp:include page ="/Views/Header.jsp"/>     

        <!--Start breadcrumb area-->     
        <section class="breadcrumb-area" style="background-image: url(./images/resources/breadcrumb-bg.jpg);">
            <div class="container">
                <div class="row">
                    <div class="col-xl-12">
                        <div class="inner-content clearfix">
                            <div class="title float-left">
                                <h2>FeedBack</h2>
                            </div>
                            <div class="breadcrumb-menu float-right">
                                <ul class="clearfix">
                                    <li><a href="Homepage.jsp">Home</a></li>
                                    <li><i class="fa fa-angle-right" aria-hidden="true"></i></li>
                                    <li class="active">FeedBack</li>
                                </ul>    
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!--End breadcrumb area-->    

        <!--Start blog area-->
        <section id="blog-area" class="blog-single-area">
            <div class="container">
                <div class="row">
                    <div class="col-xl-8 col-lg-7 col-md-12 col-sm-12">
                       <div class="add-comment-box">
                                    <div class="single-blog-title-box">
                                        <h2>Leave Your Reply</h2>
                                    </div>
                           <form id="add-comment-form" action="FeedBackDetail" method="post">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="row">
                                                    <div class="col-md-4">
                                                        <input type="text" name="fullname" value="" placeholder="Name*" required="">
                                                    </div>
                                                    <div class="col-md-4">
                                                        <input type="email" name="email" value="" placeholder="Email Address*" required="">
                                                    </div>
                                                    <div class="col-md-4">
                                                        <input type="text" name="mobile" value="" placeholder="Mobile*">
                                                    </div>
                                                     <div class="form-group">
                                                        <label class="col-sm-3" >Service</label>
                                                         <select name="category" class="col-sm-9" aria-label="Default select example">
                                                            <c:forEach items="${ListC}" var="o">
                                                                <option value="${o.id}">${o.name}</option>
                                                            </c:forEach>
                                                        </select>
                                                   </div>                           
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <textarea name="txt" placeholder="Comments*" required=""></textarea>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <button class="btn-one" type="submit">Submit Now</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                      
             <div class="col-xl-4 col-lg-5 col-md-7 col-sm-12">
                        <div class="sidebar-wrapper">
                            <!--Start single sidebar-->
                            <div class="single-sidebar">
                                <form class="search-form" action="#">
                                    <input placeholder="Search" type="text">
                                    <button type="submit"><i class="fa fa-search" aria-hidden="true"></i></button>
                                </form>
                            </div>
                            <!--End single sidebar-->
                            <!--Start single sidebar-->
                            <div class="single-sidebar">
                                <div class="sidebar-title">
                                    <h3>Categories</h3>
                                </div>
                                <ul class="categories clearfix">
                                    <li><a href="#">Healthy Teeth <span>(4)</span></a></li>
                                    <li><a href="#">Technology <span>(2)</span></a></li>
                                    <li><a href="#">Dental Care <span>(2)</span></a></li>
                                    <li><a href="#">General Dentistry <span>(5)</span></a></li>
                                    <li><a href="#">Cosmetic Dentistry <span>(4)</span></a></li>
                                    <li><a href="#">Uncategorized <span>(1)</span></a></li>
                                </ul>
                            </div>
                            <!--End single sidebar-->
                            <!--Start single sidebar--> 
                            <div class="single-sidebar">
                                <div class="sidebar-title">
                                    <h3>Recent Post</h3>
                                </div>
                                <ul class="recent-post">
                                    <li>
                                        <div class="img-holder">
                                            <img src="./images/sidebar/recent-post-1.jpg" alt="Awesome Image">
                                            <div class="overlay-style-one">
                                                <div class="box">
                                                    <div class="content">
                                                        <a href="#"><i class="fa fa-link" aria-hidden="true"></i></a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="title-holder">
                                            <p><span class="icon-date"></span>14 Nov, 2018</p>
                                            <h5 class="post-title"><a href="#">Implant care: Single tooth <br>replacement.</a></h5>
                                            <a class="readmore" href="#">Continue Reading.</a>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="img-holder">
                                            <img src="./images/sidebar/recent-post-2.jpg" alt="Awesome Image">
                                            <div class="overlay-style-one">
                                                <div class="box">
                                                    <div class="content">
                                                        <a href="#"><i class="fa fa-link" aria-hidden="true"></i></a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="title-holder">
                                            <p><span class="icon-date"></span>22 Oct, 2018</p>
                                            <h5 class="post-title"><a href="#">We are amongest most <br>qualified dentists.</a></h5>
                                            <a class="readmore" href="#">Continue Reading.</a>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="img-holder">
                                            <img src="./images/sidebar/recent-post-3.jpg" alt="Awesome Image">
                                            <div class="overlay-style-one">
                                                <div class="box">
                                                    <div class="content">
                                                        <a href="#"><i class="fa fa-link" aria-hidden="true"></i></a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="title-holder">
                                            <p><span class="icon-date"></span>05 Oct, 2018</p>
                                            <h5 class="post-title"><a href="#">If you need a crown or an <br>implant you will pay.</a></h5>
                                            <a class="readmore" href="#">Continue Reading.</a>
                                        </div>
                                    </li>

                                </ul>
                            </div>
                            <!--End single sidebar-->
                            <!--Start single-sidebar-->    
                            <div class="single-sidebar">
                                <div class="sidebar-title">
                                    <h3>Instagram</h3>
                                </div>
                                <ul class="instagram">
                                    <li>
                                        <div class="img-holder">
                                            <img src="./images/sidebar/instagram-1.jpg" alt="Awesome Image">
                                            <div class="overlay-style-one">
                                                <div class="box">
                                                    <div class="content">
                                                        <a href="#"><i class="fa fa-link" aria-hidden="true"></i></a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="img-holder">
                                            <img src="./images/sidebar/instagram-2.jpg" alt="Awesome Image">
                                            <div class="overlay-style-one">
                                                <div class="box">
                                                    <div class="content">
                                                        <a href="#"><i class="fa fa-link" aria-hidden="true"></i></a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="img-holder">
                                            <img src="./images/sidebar/instagram-3.jpg" alt="Awesome Image">
                                            <div class="overlay-style-one">
                                                <div class="box">
                                                    <div class="content">
                                                        <a href="#"><i class="fa fa-link" aria-hidden="true"></i></a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="img-holder">
                                            <img src="./images/sidebar/instagram-4.jpg" alt="Awesome Image">
                                            <div class="overlay-style-one">
                                                <div class="box">
                                                    <div class="content">
                                                        <a href="#"><i class="fa fa-link" aria-hidden="true"></i></a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </li> 
                                    <li>
                                        <div class="img-holder">
                                            <img src="./images/sidebar/instagram-5.jpg" alt="Awesome Image">
                                            <div class="overlay-style-one">
                                                <div class="box">
                                                    <div class="content">
                                                        <a href="#"><i class="fa fa-link" aria-hidden="true"></i></a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </li> 
                                    <li>
                                        <div class="img-holder">
                                            <img src="./images/sidebar/instagram-6.jpg" alt="Awesome Image">
                                            <div class="overlay-style-one">
                                                <div class="box">
                                                    <div class="content">
                                                        <a href="#"><i class="fa fa-link" aria-hidden="true"></i></a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </li> 
                                </ul>
                                <div class="follow-us-instagram">
                                    <a href="#"><i class="fa fa-instagram" aria-hidden="true"></i>Follow Us On Instagram</a>
                                </div>
                            </div>
                            <!--End single-sidebar-->
                            <!--Start single-sidebar-->
                            <div class="single-sidebar">
                                <div class="sidebar-title">
                                    <h3>Archives</h3>
                                </div>
                                <ul class="archives">
                                    <li><a href="#">August 2018 <span>(4)</span></a></li>
                                    <li><a href="#">July 2018 <span>(2)</span></a></li>
                                    <li><a href="#">March 2018 <span>(2)</span></a></li>
                                    <li><a href="#">February 2018 <span>(3)</span></a></li>
                                    <li><a href="#">December 2017 <span>(1)</span></a></li>
                                </ul>
                            </div> 
                            <!--End single-sidebar--> 
                            <!--Start single-sidebar-->
                            <div class="single-sidebar">
                                <div class="sidebar-title">
                                    <h3>Tags</h3>
                                </div>
                                <ul class="popular-tag">
                                    <li><a href="#">Checkup</a></li>
                                    <li><a href="#">Doctor</a></li>
                                    <li><a href="#">Cosmetic</a></li>
                                    <li><a href="#">Healthy Teeth</a></li>
                                    <li><a href="#">Implants</a></li>
                                    <li><a href="#">Oral Surgery</a></li>
                                    <li><a href="#">Plan & Procedures</a></li>
                                    <li><a href="#">Surgery</a></li>
                                    <li><a href="#">Teeth Filling</a></li>
                                    <li><a href="#">Tips</a></li>
                                    <li><a href="#">Teeth Whitening</a></li>
                                    <li><a href="#">Theraphy</a></li>
                                    <li><a href="#">Zygoma</a></li>
                                    <li><a href="#">3D Procedures</a></li>
                                </ul>      
                            </div> 
                            <!--End single-sidebar-->
                        </div>    
                    </div>
                </div>
        </section>

                    <!--Start sidebar Wrapper-->
                   
                    <!--End Sidebar Wrapper-->

               
        <!--End blog area--> 


        <jsp:include page="/Views/Footer.jsp"/>
        <!-- /.End Of Color Palate -->
        <jsp:include page="/Views/LinkFooter.jsp"/>


    </body>

    <!-- Mirrored from st.ourhtmldemo.com/new/Dento/blog-single.html by HTTrack Website Copier/3.x [XR&CO'2014], Tue, 05 Jan 2021 02:08:52 GMT -->
</html>
