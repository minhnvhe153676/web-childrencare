<%-- 
    Document   : Customer
    Created on : May 20, 2022, 11:39:48 PM
    Author     : Nguyen Minh Hoang
--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <title>Customer</title>

        <jsp:include page="/Views/LinkHeader.jsp"/>

        <link href="./css/CustomerCss.css" rel="stylesheet" type="text/css"/>
        <jsp:useBean id="daoPost" class="dao.PostDao" scope="request"/>
        <jsp:useBean id="daoService" class="dao.ServiceDao" scope="request"/>
        <jsp:useBean id="daoUser" class="dao.userDao" scope="request"/>
    </head>
    <body>
        <%@include file="/Views/Header.jsp" %>     

        <div class="container-xl">
            <div class="table-responsive">
                <div class="table-wrapper">
                    <div class="table-title">
                        <div class="row">
                            <div class="col-sm-5">
                                <h2>User <b>Management</b></h2>
                            </div>
                            <div class="col-sm-7">
                                <a href="CustomerDetail" class="btn btn-secondary"><span>Add New Customer</span></a>						
                            </div>
                        </div>
                    </div>
                    <table class="table table-striped table-hover">
                        <thead>
                            <tr>
                                <th>ID</th>
                                <th>Full Name</th>
                                <th>Gender</th>
                                <th>Email</th>
                                <th>Phone Number</th>
                                <th>Status</th>
                                <th>Action</th>                       
                            </tr>
                        </thead>
                        <tbody>
                            <c:forEach var="x" items="${listCustomer}">
                                <tr>
                                    <td>${x.id}</td>
                                    <td><a href="#"><img style="max-width: 4%;" src="${x.image}" class="avatar" alt="Avatar"> ${x.fullName}</a></td>
                                    <td>
                                        <c:choose>
                                            <c:when test="${x.gender}">
                                                Male
                                            </c:when>                            
                                            <c:otherwise>
                                                Female
                                            </c:otherwise>                         
                                        </c:choose>
                                    </td>                        
                                    <td>${x.email}</td>
                                    <td>${x.mobile}</td>
                                    <td><span class="status 
                                              <c:choose>
                                                  <c:when test="${x.status == 1}">
                                                      text-success ">&bull;</span> Active </td>
                                              </c:when> 
                                              <c:when test="${x.status == 2}">
                                              text-warning ">&bull;</span> Inactive</td>
                                        </c:when>
                                        <c:otherwise>
                                            text-danger ">&bull;</span> Suspended</td>
                                        </c:otherwise>                         
                                    </c:choose>
                                    <td>
                                        <a href="CustomerDetail?id=${x.id}" class="settings" title="Settings" data-toggle="tooltip"><i class="fa fa-cog" aria-hidden="true"></i></a>
                                        <a href="CustomerDetail?id=${x.id}&action=delete" class="delete" title="Delete" data-toggle="tooltip"><i class="fa fa-trash" aria-hidden="true"></i></a>
                                    </td>
                                </tr>
                            </c:forEach>


                        </tbody>
                    </table>
                    <div class="clearfix">
                        <div class="hint-text">Showing <b>
                                <c:choose>
                                    <c:when test="${daoUser.totalCustomer<5}">
                                        ${daoUser.totalCustomer}
                                    </c:when>
                                    <c:otherwise>
                                        5
                                    </c:otherwise>
                                </c:choose>
                            
                                </b> out of <b>${daoUser.totalCustomer}</b> entries</div>
                        <ul class="pagination">
                            <li class="page-item disabled"><a <c:if test="${check == 0}">style="pointer-events: none"</c:if>  href="Customer?action=previous&index=${check-1}">Previous</a></li>
                            <c:forEach begin="${start}" end="${end}" varStatus="loop">
                            <li class="page-item <c:if test="${check==loop.index}">active</c:if>"><a href="Customer?index=${loop.index}" <c:if test="${disable==1}">style="pointer-events: none"</c:if> class="page-link">${loop.index+1}</a></li>
                            </c:forEach>                           
                            <li class="page-item"><a href="Customer?action=next&index=${check}" <c:if test="${disable==1}">style="pointer-events: none"</c:if>  class="page-link">Next</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>     

       
        <%@include file="/Views/Footer.jsp" %>
        <jsp:include page="/Views/LinkFooter.jsp"/>
    </body>
</html>