<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<div class="col-xl-4 col-lg-4 col-md-6 col-sm-12">
    <div class="sidebar-wrapper">
        <!--Start single sidebar-->
        <div class="single-sidebar">
            <form class="search-form" action="Post" method="post">
                <input oninput="searchPosts(this)" name="postTitle" placeholder="Search" type="text">
                <button type="submit"><i class="fa fa-search" aria-hidden="true"></i></button>
            </form>
        </div>
        <!--End single sidebar-->
        <!--Start single sidebar-->
        <div class="single-sidebar">
            <div class="sidebar-title">
                <h3>Categories</h3>
            </div>
            <ul class="categories clearfix">
                <c:forEach var="x" items="${daoPost.categoryPost}">
                    <li><a href="Post?categoryId=${x.id}">${x.name} </a></li>
                    </c:forEach>       
            </ul>
        </div>
        <!--End single sidebar-->
        <!--Start single sidebar--> 
        <div class="single-sidebar">
            <div class="sidebar-title">
                <h3>Recent Post</h3>
            </div>
            <ul class="recent-post">
                <c:forEach var="x" items="${daoPost.posts}">
                    <li>
                        <div class="img-holder">
                            <img src="${x.thumbnailLink}" alt="Awesome Image">
                            <div class="overlay-style-one">
                                <div class="box">
                                    <div class="content">
                                        <a href="PostDetail?index=${x.id}"><i class="fa fa-link" aria-hidden="true"></i></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="title-holder">
                            <p><span class="icon-date"></span>${x.datePublic2}</p>
                            <h5 class="post-title"><a href="PostDetail?index=${x.id}">${x.title}</h5>
                            <a class="readmore" href="PostDetail?index=${x.id}">Continue Reading...</a>
                        </div>
                    </li>
                </c:forEach>

                

            </ul>
        </div>
        <!--End single sidebar-->

        <!--Start single-sidebar-->
        <%--<div class="single-sidebar">
            <div class="sidebar-title">
                <h3>Archives</h3>
            </div>
            <ul class="archives">
                <li><a href="#">August 2018</a></li>
                <li><a href="#">July 2018 </a></li>
                <li><a href="#">March 2018 </a></li>
                <li><a href="#">February 2018 </a></li>
                <li><a href="#">December 2017 </a></li>
            </ul>
        </div> 
        <!--End single-sidebar--> 
        <!--Start single-sidebar-->
        <div class="single-sidebar">
            <div class="sidebar-title">
                <h3>Tags</h3>
            </div>
            <ul class="popular-tag">
                <li><a href="#">Checkup</a></li>
                <li><a href="#">Doctor</a></li>
                <li><a href="#">Cosmetic</a></li>
                <li><a href="#">Healthy Teeth</a></li>
                <li><a href="#">Implants</a></li>
                <li><a href="#">Oral Surgery</a></li>
                <li><a href="#">Plan & Procedures</a></li>
                <li><a href="#">Surgery</a></li>
                <li><a href="#">Teeth Filling</a></li>
                <li><a href="#">Tips</a></li>
                <li><a href="#">Teeth Whitening</a></li>
                <li><a href="#">Theraphy</a></li>
                <li><a href="#">Zygoma</a></li>
                <li><a href="#">3D Procedures</a></li>
            </ul>      
        </div> --%>
        <!--End single-sidebar-->
    </div>    
</div>
<!--End Sidebar Wrapper-->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script>

    function searchPosts(para) {
        var textSearch = para.value;

        $.ajax({
            url: "/ChildrenCare/SearchPostByTittle",
            type: 'get',
            data: {
                txt: textSearch

            },
            success: function (response) {
                var row = document.getElementById("content");
                row.innerHTML = response;
            },
            error: function (xhr) {
                console.log(xhr);

            }
        });
    }
</script>

