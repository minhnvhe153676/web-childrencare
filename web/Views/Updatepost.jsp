<%-- 
    Document   : Updatepost
    Created on : Jul 20, 2022, 1:32:30 AM
    Author     : Admin
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <jsp:include page="/Views/LinkHeader.jsp"/>
        <jsp:useBean id="daoPost" class="dao.PostDao" scope="request"/>
        <jsp:useBean id="daoService" class="dao.ServiceDao" scope="request"/>
        <title>JSP Page</title>
    </head>
    <body>
        <%@include file="/Views/Header.jsp" %>      
        <section class="breadcrumb-area" style="background-image: url(./images/resources/breadcrumb-bg.jpg);">
            <div class="container">
                <div class="row">
                    <div class="col-xl-12">
                        <div class="inner-content clearfix">
                            <div class="title float-left">
                                <h2>ManagePost</h2>
                            </div>
                            <div class="breadcrumb-menu float-right">
                                <ul class="clearfix">
                                    <li><a href="HomePage">Home</a></li>
                                    <li><i class="fa fa-angle-right" aria-hidden="true"></i></li>
                                    <li class="active">Post</li>
                                </ul>    
                            </div>
                        </div>
                        <div>
                            <!--                                <a class="fa-solid fa-plus" style="text-align: center"></a>-->
                          
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <div   id="EditPostModal">
                                    <div class="modal-dialog">
                                        <div class="modal-content">
                                            <form action="Updatepost1" method="post">
                                                <div class="modal-header">						
                                                    <h4  class="modal-title">Edit Post</h4>
                                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                                </div>
                                                <div class="modal-body">					
                                                    <div class="form-group">
                                                        <label>Name Author</label>
                                                        <input value="${post.author}" name="author" type="text" class="form-control" required placeholder="Name Author">
                                                    </div>
                                                    <div class="form-group">
                                                        <label>CategoryPost</label>
                                                        <select name="categoryid" class="form-select form-control" aria-label="Default select example">
                                                            <c:forEach items="${listc}" var="o">
                                                                <option ${post.category.id==o.id ? "selected" : ""} value="${o.id}" >${o.name}</option>
                                                            </c:forEach>
                                                        </select>
                                                    </div>
                                                    <div class="form-group">
                                                        <label>Content</label>
                                                        <textarea value="${post.content}" name="content" type="text" class="form-control" required placeholder="Content">${post.content}</textarea>
                                                    </div>
                                                    <div class="form-group">
                                                        <label>Title</label>
                                                        <textarea value="${post.title}" name="title" type="text" class="form-control" required placeholder="title">${post.title}</textarea>
                                                    </div>
                                                    <div class="form-group">
                                                        <label>description</label>
                                                        <textarea value="${post.description}" name="description" type="text" class="form-control" required placeholder="description">${post.description}</textarea>
                                                    </div>
                                                    <div class="form-group">
                                                        <label>thumbnailLink</label>
                                                        <input  value="${post.thumbnailLink}" name="thumbnailLink" type="text" class="form-control" required placeholder="Linkimage">
                                                    </div>
                                                    <div class="form-group">
                                                        <label>updatedate</label>
                                                        <input value="${post.convertUpdateDate}" name="updatedate" type="date" class="form-control" required placeholder="Update date">
                                                    </div>




                                                </div>
                                                    
                                                <div class="modal-footer">
                                                    <input type="button" class="btn btn-default" data-dismiss="modal" value="Huỷ">
                                                    <input type="submit" class="btn btn-success" value="edit">
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
             <%@include file="/Views/Footer.jsp" %>
             <jsp:include page="/Views/LinkFooter.jsp"/>
    </body>
</html>
