<%@ taglib prefix = "fmt" uri = "http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <title>Blog</title>

        <jsp:include page="/Views/LinkHeader.jsp"/>
        <jsp:useBean id="daoPost" class="dao.PostDao" scope="request"/>
        <jsp:useBean id="daoService" class="dao.ServiceDao" scope="request"/>
    </head>

    <body>
        <%@include file="/Views/Header.jsp" %>      
        <section class="breadcrumb-area" style="background-image: url(./images/resources/breadcrumb-bg.jpg);">
            <div class="container">
                <div class="row">
                    <div class="col-xl-12">
                        <div class="inner-content clearfix">
                            <div class="title float-left">
                                <h2>ManagePost</h2>
                            </div>
                            <div class="breadcrumb-menu float-right">
                                <ul class="clearfix">
                                    <li><a href="HomePage">Home</a></li>
                                    <li><i class="fa fa-angle-right" aria-hidden="true"></i></li>
                                    <li class="active">Post</li>
                                </ul>    
                            </div>
                        </div>
                        <div>
                            <!--                                <a class="fa-solid fa-plus" style="text-align: center"></a>-->
                            <a href="#addPostModal"  class="btn btn-success" data-toggle="modal"><i class="fa-solid fa-plus"></i> <span>Add Post</span></a>

                        </div>
                    </div>
                </div>
            </div>
        </section>
        <section id="blog-area" class="blog-default-area">

            <div class="container" >
                <div class="row" >
                    <div id="content" class="col-xl-8 col-lg-8 col-md-12 col-sm-12"style="display: flex;flex-wrap: wrap;" >   

                        <c:forEach var="x" items="${listPost}" >                 
                            <div class="single-blog-post col-xl-6  col-lg-8 col-md-12 col-sm-12" style="justify-content: space-between;margin-bottom: 0; "   >
                                <div class="icon">
                                <a href="" class="fa-solid fa-eye"></a>
                                <a href="Updatepost1?pid=${x.id}" class="edit"><i class="fa-solid fa-pen" title="Edit"></i> </a>
                                </div>
                                <div class="img-holder" style="width: 100%; height: 200px;  ">

                                    <img src="${x.thumbnailLink}" alt="Awesome Image">
                                    <div class="categorie-button">
                                        <a class="btn-one" href="#">${x.category.name}</a>    
                                    </div>

                                </div>

                                <div class="text-holder"style="width: 100%; height: 400px;"> 
                                    <div class="meta-box">
                                        <div class="author-thumb">
                                            <img src="./images/blog/author-2.png" alt="Image">
                                        </div>
                                        <ul class="meta-info">
                                            <li><a href="#">${x.author}</a></li>
                                            <li><a href="#">${x.datePublic2}</a></li>
                                        </ul>    
                                    </div>
                                        
                                    <h3 class="blog-title"><a href="PostDetail?index=${x.id}">${x.title}</a></h3> 
                                    <div class="text-box">
                                        <p>${x.content}</p>
                                    </div>
                                     
                                </div>   
                            </div>

                        </c:forEach>
                    </div>
                    <!-- sidebar Wrapper-->
                    <%@include file="/Views/SidebarWrapper.jsp" %> 


                    <div class="clearfix">
                    <ul class="pagination">
                        <c:if test="${indexPage != 1}">
                            <li class="page-item"><a href="ManagerPostList1?Index=${indexPage-1}">Trước</a></li>
                            </c:if>

                        <c:forEach begin="1" end="${numberOfPage}" var="o">
                            <li class="page-item ${indexPage == o?"active":""}"><a href="ManagerPostList1?Index=${o}" class="page-link">${o}</a></li>
                            </c:forEach>

                        <c:if test="${indexPage != numberOfPage}">
                            <li class="page-item"><a href="ManagerPostList1?Index=${indexPage+1}" class="page-link">Sau</a></li>
                            </c:if>

                    </ul>
                </div>
                </div>
            </div>
        </section>
                       
        <div id="addPostModal" class="modal fade">
            <div class="modal-dialog">
                <div class="modal-content">
                    <form action="ManagerPostList1" method="post">
                        <div class="modal-header">						
                            <h4  class="modal-title">Add Post</h4>
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        </div>
                        <div class="modal-body">					

                            <div class="form-group">
                                <label>Name Author</label>
                                <input name="author" type="text" class="form-control" required placeholder="Name Author">
                            </div>
                            <div class="form-group">
                                <label>CategoryPost</label>
                                <select name="categoryid" class="form-select form-control" aria-label="Default select example">
                                    <c:forEach items="${listc}" var="o">
                                        <option value="${o.id}">${o.name}</option>
                                    </c:forEach>
                                </select>
                            </div>
                            <div class="form-group">
                                <label>Content</label>
                                <input name="content" type="text" class="form-control" required placeholder="Content">
                            </div>
                            <div class="form-group">
                                <label>description</label>
                                <input name="description" type="text" class="form-control" required placeholder="description">
                            </div>
                            <div class="form-group">
                                <label>thumbnailLink</label>
                                <input name="thumbnailLink" type="text" class="form-control" required placeholder="Linkimage">
                            </div>
                             <div class="form-group">
                                <label>Title</label>
                                <input name="title" type="text" class="form-control" required placeholder="Title">
                            </div>
                            <div class="form-group">
                                <label>updatedate</label>
                                <input name="updatedate" type="date" class="form-control" required placeholder="Update date">
                            </div>




                        </div>
                        <div class="modal-footer">
                            <input type="button" class="btn btn-default" data-dismiss="modal" value="Huỷ">
                            <input type="submit" class="btn btn-success" value="Thêm">
                        </div>
                    </form>
                </div>
            </div>
        </div>
   
        <%@include file="/Views/Footer.jsp" %>
        <jsp:include page="/Views/LinkFooter.jsp"/>
    </body>

</html>
